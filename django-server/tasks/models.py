from django.db import models


class Task(models.Model):
	title = models.CharField(max_length=255)
	priority = models.CharField(max_length=40)
	category = models.CharField(max_length=40)


class SimpleTask(models.Model):
	title = models.CharField(max_length=255)
	main_task = models.ForeignKey(Task, on_delete=models.CASCADE)

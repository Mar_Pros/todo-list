from rest_framework import serializers

from .models import Task, SimpleTask


class TaskSerializer(serializers.ModelSerializer):
	class Meta:
		model = Task
		fields = ('id', 'title', 'category', 'priority')


class SimpleTaskSerializer(serializers.ModelSerializer):
	class Meta:
		model = SimpleTask
		fields = ('id', 'title', 'main_task')

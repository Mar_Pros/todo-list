from django.shortcuts import render

from .models import Task, SimpleTask
from .serializers import TaskSerializer, SimpleTaskSerializer

from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend


class TaskViewSet(viewsets.ModelViewSet):
	queryset = Task.objects.all()
	serializer_class = TaskSerializer

	# @action(methods=['post'], detail=True)
	# def subtasks(self, request):
	# 	task = self.get_object()
	# 	subtasks = SimpleTask.objects.filter(main_task=task)
	# 	subtasks_json = SimpleTaskSerializer(subtasks, many=True)
	# 	return Response(subtasks_json.data)



class SimpleTaskViewSet(viewsets.ModelViewSet):
	queryset = SimpleTask.objects.all()
	serializer_class = SimpleTaskSerializer
	filter_backends = [DjangoFilterBackend]
	filterset_fields = ['main_task']






# def get_simple_tasks(id):
# 	queryset = SimpleTask.objects.get(main_task_id=id)
# 	serializer_class = SimpleTaskSerializer